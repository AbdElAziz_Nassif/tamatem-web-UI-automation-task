package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class GameDetailsPage extends BasePage{


    public GameDetailsPage(WebDriver driver) {
        super(driver);
    }
    By gameNameFromBanner = By.cssSelector(".banner .name");

    public String getGameNameFromMainBanner ()
    {
        return getTextOfElement(gameNameFromBanner) ;
    }

}
