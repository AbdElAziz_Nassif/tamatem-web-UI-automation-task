package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static filesReaders.ReadFromFiles.getPropertyByKey;

public class HomePage extends BasePage{

    // constructor
    public HomePage(WebDriver driver) {
        super(driver);
    }

    // locators of home page
    By addNewAccountBtn = By.cssSelector(".header [routerlink='/sign-up/by/phone']");
    By gamesIcon = By.cssSelector(".header [routerlink='/games']");

    // Operations on homa page web elements
    public HomePage navigateToHomePage ()
    {
        navigateToURL(getPropertyByKey("environment.properties", "APP_URL") + "home");
        return this;
    }
    public RegistrationPage clickAddNewAccountFromHeader()
    {
        clickElement(addNewAccountBtn);
        return new RegistrationPage(driver);
    }
    public GamesListPage clickGamesFromHeader ()
    {
        clickElement(gamesIcon);
        return new GamesListPage(driver);
    }
}
