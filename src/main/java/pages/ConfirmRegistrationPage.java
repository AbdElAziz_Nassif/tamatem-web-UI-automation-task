package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ConfirmRegistrationPage extends BasePage{

    public ConfirmRegistrationPage(WebDriver driver) {
        super(driver);
    }
    By phoneConfirmationMessage = By.cssSelector(".mt-12");
    By OTPMessage = By.cssSelector(".mt-3");

    public String getPhoneConfirmationMessage ()
    {
        return getTextOfElement(phoneConfirmationMessage);
    }
    public String getOTPMessage ()
    {
        return getTextOfElement(OTPMessage);
    }
}
