package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class RegistrationPage extends BasePage{
    // constructor
    public RegistrationPage(WebDriver driver) {
        super(driver);
    }

    // locators
    By phoneField = By.id("phone");
    By passwordField = By.id("mat-input-0");
    By fullNameField = By.id("mat-input-1");
    By emailField = By.id("mat-input-2");
    By submitButton = By.cssSelector("button[mat-raised-button]");

    //operation on web elements
    public ConfirmRegistrationPage registerNewUSer (String phone, String password, String fullName, String email)
    {
        typeOnInputField(phoneField,phone);
        typeOnInputField(passwordField, password);
        typeOnInputField(fullNameField, fullName);
        typeOnInputField(emailField, email);
        clickElement(submitButton);
        return new ConfirmRegistrationPage(driver);
    }




}
