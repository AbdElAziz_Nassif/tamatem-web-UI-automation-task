package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class GamesListPage extends BasePage{


    public GamesListPage(WebDriver driver) {
        super(driver);
    }

    By gamesEntryButton = By.cssSelector("button[routerlink='/games/details']");
    By gameName = By.xpath("(//div[contains(@class,'name')])[1]");

    // gameIndex should
    public String getGameName ()
    {
        return getTextOfElement(gameName) ;
    }

    public GameDetailsPage clickGameEntryBtn ()
    {
        forceClickOnElement(gamesEntryButton);
        return new GameDetailsPage(driver);
    }

}
