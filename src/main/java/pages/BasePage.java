package pages;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;
import java.util.NoSuchElementException;

public class BasePage {

    WebDriver driver;
    WebDriverWait wait;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, Duration.ofSeconds(30));
    }

    protected WebElement locateElement(By elementLocator) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(elementLocator));
        wait.until(ExpectedConditions.elementToBeClickable(elementLocator));
        return driver.findElement(elementLocator);
    }

    protected List<WebElement> locateListOfElement(By elementsLocator) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(elementsLocator));
        wait.until(ExpectedConditions.elementToBeClickable(elementsLocator));
        return driver.findElements(elementsLocator);
    }

    protected void clickElement(By elementLocator) {
        try {
            locateElement(elementLocator).click();
            waitUntilPageToLoad();
        }
        catch (NoSuchElementException e)
        {
            locateElement(elementLocator).click();
            waitUntilPageToLoad();
        }
    }

    protected void typeOnInputField(By elementLocator, String text) {
        locateElement(elementLocator).sendKeys(text);
    }

    protected String getTextOfElement(By elementLocator) {
        try {
            waitUntilPageToLoad();
            return locateElement(elementLocator).getText();
        }
        catch (NoSuchElementException e)
        {
            waitUntilPageToLoad();
            return locateElement(elementLocator).getText();
        }

    }

    protected String getAttributeOfElement(By elementLocator, String attributeKey) {
        return locateElement(elementLocator).getAttribute(attributeKey);
    }

    protected void hoverOverElement(By elementLocator) {
        Actions actions = new Actions(driver);
        actions.moveToElement(locateElement(elementLocator));
        actions.perform();
    }

    protected void scrollVertically (int y_pixcel)
    {
        Actions actions = new Actions(driver);
        actions.scrollByAmount(0, y_pixcel);
        actions.perform();
    }
    protected void scrollToElement (By elementLocator)
    {
        WebElement element = locateElement(elementLocator);
        Actions actions = new Actions(driver);
        actions.scrollToElement(element);
    }
    protected void forceClickOnElement (By elementLocator )
    {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        WebElement element = locateElement(elementLocator) ;
        js.executeScript("arguments[0].click()" , element) ;
    }

    protected void navigateToURL (String url)
    {
        driver.get(url);
    }

    public void waitUntilPageToLoad ()
    {
        JavascriptExecutor j = (JavascriptExecutor)driver;
        if (j.executeScript("return document.readyState").toString().equals("complete")){
            System.out.println("Page has loaded");
        }
    }
}
