package tests.registrationTests;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.ConfirmRegistrationPage;
import pages.RegistrationPage;
import tests.TestBase;
import static filesReaders.ReadFromFiles.getJsonStringValueByKey;

public class RegistrationTests extends TestBase {
    String phone;
    String password;
    String fullName;
    String email;

    @BeforeClass
    public void getTestData()
    {
        // generate random phone number each time
        phone = getJsonStringValueByKey("RegistrationTestData.json", "phoneNumber");
        phone = String.format(phone, String.valueOf(System.currentTimeMillis()).substring(4, 12));
        password = getJsonStringValueByKey("RegistrationTestData.json", "password");
        fullName = getJsonStringValueByKey("RegistrationTestData.json", "fullName");
        // generate random email number each time
        email = getJsonStringValueByKey("RegistrationTestData.json", "email");
        email = String.format(email, System.currentTimeMillis());
    }

    @Test
    public void testRegisterNewUser() {

        RegistrationPage registrationPage = homePage.clickAddNewAccountFromHeader();
        ConfirmRegistrationPage confirmRegistrationPage = registrationPage.registerNewUSer(phone,password,fullName,email);
        Assert.assertEquals(confirmRegistrationPage.getPhoneConfirmationMessage(), "تأكيد رقم الهاتف",
                "Phone confirmation message does not appear");
        Assert.assertTrue(confirmRegistrationPage.getOTPMessage().contains(phone),
                "OTP message does not contain phone number");
    }
}
