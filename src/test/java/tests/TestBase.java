package tests;

import driverSettigns.DriverFactory;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import pages.HomePage;

public class TestBase {

    protected WebDriver driver;
    protected HomePage homePage;

    @BeforeMethod
    public void setup () {
        driver=DriverFactory.getDriver() ;
        // start of application
        homePage=new HomePage(driver);
        homePage.navigateToHomePage();
        driver.manage().window().maximize();
    }

    @AfterMethod
    public void teardown ()
    {
        driver.quit();
    }
}
