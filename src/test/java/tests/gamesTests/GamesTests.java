package tests.gamesTests;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.ConfirmRegistrationPage;
import pages.GameDetailsPage;
import pages.GamesListPage;
import pages.RegistrationPage;
import tests.TestBase;

import static filesReaders.ReadFromFiles.getJsonStringValueByKey;

public class GamesTests extends TestBase {

    @Test
    public void testEnteringGame() {
        GamesListPage gamesListPage = homePage.clickGamesFromHeader();
        String gameName = gamesListPage.getGameName();
        GameDetailsPage gameDetailsPage = gamesListPage.clickGameEntryBtn();
        Assert.assertEquals(gameName, gameDetailsPage.getGameNameFromMainBanner(),
                "The selected game name is not as expected");
    }
}
